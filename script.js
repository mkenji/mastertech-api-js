function mostrar(dados) {
    let divResposta = document.querySelector("#resultado");

    while (divResposta.firstChild) {
        divResposta.removeChild(divResposta.firstChild);
    }

    if(dados.error_message){
        let htmlErro = document.createElement("p");
        htmlErro.innerHTML = "Erro ao obter os dados do servidor.";

        divResposta.appendChild(htmlErro);
    }
    else{
        console.log(dados);

        let htmlNome = "";
        let htmlDomain = "";
        let htmlLogo = "";
        
        for (let i in dados) {
            htmlDivItens = document.createElement("div");
            htmlDivItens.style.border = "solid 1px black";

            htmlNome = document.createElement("p");
            htmlNome.innerHTML = "Nome da empresa: "+dados[i].name;
            htmlDomain = document.createElement("p");
            htmlDomain.innerHTML = "URL da empresa: <a target='_blank' href='http://"+dados[i].domain+"'>"+dados[i].domain+'</a>';
            htmlLogo = document.createElement("img");
            htmlLogo.src = dados[i].logo;

            htmlDivItens.appendChild(htmlNome);
            htmlDivItens.appendChild(htmlDomain);
            htmlDivItens.appendChild(htmlLogo);

            divResposta.appendChild(htmlDivItens);
        }

       
        /*
        let latitude = dados.results[0].geometry.location.lat;
        let longitude = dados.results[0].geometry.location.lng;
        let nomeCidade = dados.results[0].formatted_address;

        let htmlNome = document.createElement("p");
        htmlNome.innerHTML = "Cidade: " + nomeCidade;

        let htmlLatitude = document.createElement("p");
        htmlLatitude.innerHTML = "Latitude: "+ latitude;
        let htmlLongitude = document.createElement("p");
        htmlLongitude.innerHTML = "Longitude: "+ longitude;


        divResposta.appendChild(htmlNome);
        divResposta.appendChild(htmlLatitude);
        divResposta.appendChild(htmlLongitude);
        */
    }
}

function extrair(resposta) {
    if(resposta.ok){
        return resposta.json();
    }
}

function buscarLatLong(){
    let boxInput = document.querySelector("input");
    let empresa = boxInput.value;
    const baseUrl = "https://autocomplete.clearbit.com/v1/companies/suggest";
    let url = baseUrl + "?query=:" + empresa;
    fetch(url).then(extrair).then(mostrar);
}

let botao = document.querySelector("button");
botao.onclick = buscarLatLong;
